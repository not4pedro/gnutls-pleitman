From 85a881cbca6f8e8578af7a026163ac3075ea267c Mon Sep 17 00:00:00 2001
From: Daiki Ueno <ueno@gnu.org>
Date: Mon, 21 Feb 2022 16:28:49 +0100
Subject: [PATCH] priority: compile out GOST algorithms IDs if they are
 disabled

When compiled with --disable-gost, gnutls-cli --priority NORMAL --list
still prints GOST algorithms for ciphers, MACs, and signatures.  This
change adds compile time checks to suppress them.

Signed-off-by: Daiki Ueno <ueno@gnu.org>
---
 lib/priority.c | 9 ++++++++-
 1 file changed, 8 insertions(+), 1 deletion(-)

diff --git a/lib/priority.c b/lib/priority.c
index 54d7b1bb45..0c7ac65d7b 100644
--- a/lib/priority.c
+++ b/lib/priority.c
@@ -309,7 +309,9 @@ static const int _kx_priority_secure[] = {
 static const int* kx_priority_secure = _kx_priority_secure;
 
 static const int _kx_priority_gost[] = {
+#ifdef ENABLE_GOST
 	GNUTLS_KX_VKO_GOST_12,
+#endif
 	0,
 };
 static const int* kx_priority_gost = _kx_priority_gost;
@@ -507,9 +509,10 @@ static const int _sign_priority_secure192[] = {
 static const int* sign_priority_secure192 = _sign_priority_secure192;
 
 static const int _sign_priority_gost[] = {
+#ifdef ENABLE_GOST
 	GNUTLS_SIGN_GOST_256,
 	GNUTLS_SIGN_GOST_512,
-
+#endif
 	0
 };
 static const int* sign_priority_gost = _sign_priority_gost;
@@ -531,13 +534,17 @@ static const int *cipher_priority_normal = _cipher_priority_normal_default;
 static const int *mac_priority_normal = mac_priority_normal_default;
 
 static const int _cipher_priority_gost[] = {
+#ifdef ENABLE_GOST
 	GNUTLS_CIPHER_GOST28147_TC26Z_CNT,
+#endif
 	0
 };
 static const int *cipher_priority_gost = _cipher_priority_gost;
 
 static const int _mac_priority_gost[] = {
+#ifdef ENABLE_GOST
 	GNUTLS_MAC_GOST28147_TC26Z_IMIT,
+#endif
 	0
 };
 static const int *mac_priority_gost = _mac_priority_gost;
-- 
2.34.1

From f2bb30f68922d72f8bed29cc8b2a065087a0969f Mon Sep 17 00:00:00 2001
From: Daiki Ueno <ueno@gnu.org>
Date: Tue, 22 Feb 2022 17:09:46 +0100
Subject: [PATCH] algorithms: ensure _list() exclude non-existing algorithms

This aligns the behavior of _list() function for sign/pk to the one
for cipher/mac: the former previously returned all the algorithms
defined, while the latter returns only algorithms compiled in.

Signed-off-by: Daiki Ueno <ueno@gnu.org>
---
 lib/algorithms/publickey.c |  8 +++-
 lib/algorithms/sign.c      |  4 +-
 lib/crypto-backend.h       |  2 +
 lib/nettle/pk.c            | 86 ++++++++++++++++++++++++++++++++++++++
 lib/pk.h                   |  2 +
 5 files changed, 99 insertions(+), 3 deletions(-)

diff --git a/lib/algorithms/publickey.c b/lib/algorithms/publickey.c
index b4cd6b1df0..caf53972ab 100644
--- a/lib/algorithms/publickey.c
+++ b/lib/algorithms/publickey.c
@@ -24,6 +24,7 @@
 #include <algorithms.h>
 #include "errors.h"
 #include <x509/common.h>
+#include "pk.h"
 
 
 /* KX mappings to PK algorithms */
@@ -203,8 +204,11 @@ const gnutls_pk_algorithm_t *gnutls_pk_list(void)
 		int i = 0;
 
 		GNUTLS_PK_LOOP(
-			if (p->id != GNUTLS_PK_UNKNOWN && supported_pks[i > 0 ? (i - 1) : 0] != p->id)
-				supported_pks[i++] = p->id
+			if (p->id != GNUTLS_PK_UNKNOWN &&
+			    supported_pks[i > 0 ? (i - 1) : 0] != p->id &&
+			    _gnutls_pk_exists(p->id)) {
+				supported_pks[i++] = p->id;
+			}
 		);
 		supported_pks[i++] = 0;
 	}
diff --git a/lib/algorithms/sign.c b/lib/algorithms/sign.c
index 06abdb4cf8..4a5aaa75e1 100644
--- a/lib/algorithms/sign.c
+++ b/lib/algorithms/sign.c
@@ -27,6 +27,7 @@
 #include <x509/common.h>
 #include <assert.h>
 #include "c-strcase.h"
+#include "pk.h"
 
 /* signature algorithms;
  */
@@ -631,7 +632,8 @@ const gnutls_sign_algorithm_t *gnutls_sign_list(void)
 
 		GNUTLS_SIGN_LOOP(
 			/* list all algorithms, but not duplicates */
-			if (supported_sign[i] != p->id) {
+			if (supported_sign[i] != p->id &&
+			    _gnutls_pk_sign_exists(p->id)) {
 				assert(i+1 < MAX_ALGOS);
 				supported_sign[i++] = p->id;
 				supported_sign[i+1] = 0;
diff --git a/lib/crypto-backend.h b/lib/crypto-backend.h
index 9874033221..f0f68c337e 100644
--- a/lib/crypto-backend.h
+++ b/lib/crypto-backend.h
@@ -418,6 +418,8 @@ typedef struct gnutls_crypto_pk {
 		       unsigned int flags);
 
 	int (*curve_exists) (gnutls_ecc_curve_t);	/* true/false */
+	int (*pk_exists) (gnutls_pk_algorithm_t);	/* true/false */
+	int (*sign_exists) (gnutls_sign_algorithm_t);	/* true/false */
 } gnutls_crypto_pk_st;
 
 /* priority: infinity for backend algorithms, 90 for kernel
diff --git a/lib/nettle/pk.c b/lib/nettle/pk.c
index a146568266..eba246f0b3 100644
--- a/lib/nettle/pk.c
+++ b/lib/nettle/pk.c
@@ -1883,6 +1883,90 @@ static int _wrap_nettle_pk_curve_exists(gnutls_ecc_curve_t curve)
 	}
 }
 
+static int _wrap_nettle_pk_exists(gnutls_pk_algorithm_t pk)
+{
+	switch (pk) {
+	case GNUTLS_PK_RSA:
+	case GNUTLS_PK_DSA:
+	case GNUTLS_PK_DH:
+	case GNUTLS_PK_ECDSA:
+	case GNUTLS_PK_ECDH_X25519:
+	case GNUTLS_PK_RSA_PSS:
+	case GNUTLS_PK_EDDSA_ED25519:
+#if ENABLE_GOST
+	case GNUTLS_PK_GOST_01:
+	case GNUTLS_PK_GOST_12_256:
+	case GNUTLS_PK_GOST_12_512:
+#endif
+	case GNUTLS_PK_ECDH_X448:
+	case GNUTLS_PK_EDDSA_ED448:
+		return 1;
+	default:
+		return 0;
+	}
+}
+
+static int _wrap_nettle_pk_sign_exists(gnutls_sign_algorithm_t sign)
+{
+	switch (sign) {
+	case GNUTLS_SIGN_RSA_SHA1:
+	case GNUTLS_SIGN_DSA_SHA1:
+	case GNUTLS_SIGN_RSA_MD5:
+	case GNUTLS_SIGN_RSA_MD2:
+	case GNUTLS_SIGN_RSA_RMD160:
+	case GNUTLS_SIGN_RSA_SHA256:
+	case GNUTLS_SIGN_RSA_SHA384:
+	case GNUTLS_SIGN_RSA_SHA512:
+	case GNUTLS_SIGN_RSA_SHA224:
+	case GNUTLS_SIGN_DSA_SHA224:
+	case GNUTLS_SIGN_DSA_SHA256:
+	case GNUTLS_SIGN_ECDSA_SHA1:
+	case GNUTLS_SIGN_ECDSA_SHA224:
+	case GNUTLS_SIGN_ECDSA_SHA256:
+	case GNUTLS_SIGN_ECDSA_SHA384:
+	case GNUTLS_SIGN_ECDSA_SHA512:
+	case GNUTLS_SIGN_DSA_SHA384:
+	case GNUTLS_SIGN_DSA_SHA512:
+	case GNUTLS_SIGN_ECDSA_SHA3_224:
+	case GNUTLS_SIGN_ECDSA_SHA3_256:
+	case GNUTLS_SIGN_ECDSA_SHA3_384:
+	case GNUTLS_SIGN_ECDSA_SHA3_512:
+
+	case GNUTLS_SIGN_DSA_SHA3_224:
+	case GNUTLS_SIGN_DSA_SHA3_256:
+	case GNUTLS_SIGN_DSA_SHA3_384:
+	case GNUTLS_SIGN_DSA_SHA3_512:
+	case GNUTLS_SIGN_RSA_SHA3_224:
+	case GNUTLS_SIGN_RSA_SHA3_256:
+	case GNUTLS_SIGN_RSA_SHA3_384:
+	case GNUTLS_SIGN_RSA_SHA3_512:
+
+	case GNUTLS_SIGN_RSA_PSS_SHA256:
+	case GNUTLS_SIGN_RSA_PSS_SHA384:
+	case GNUTLS_SIGN_RSA_PSS_SHA512:
+	case GNUTLS_SIGN_EDDSA_ED25519:
+	case GNUTLS_SIGN_RSA_RAW:
+
+	case GNUTLS_SIGN_ECDSA_SECP256R1_SHA256:
+	case GNUTLS_SIGN_ECDSA_SECP384R1_SHA384:
+	case GNUTLS_SIGN_ECDSA_SECP521R1_SHA512:
+
+	case GNUTLS_SIGN_RSA_PSS_RSAE_SHA256:
+	case GNUTLS_SIGN_RSA_PSS_RSAE_SHA384:
+	case GNUTLS_SIGN_RSA_PSS_RSAE_SHA512:
+
+#if ENABLE_GOST
+	case GNUTLS_SIGN_GOST_94:
+	case GNUTLS_SIGN_GOST_256:
+	case GNUTLS_SIGN_GOST_512:
+#endif
+	case GNUTLS_SIGN_EDDSA_ED448:
+		return 1;
+	default:
+		return 0;
+	}
+}
+
 /* Generates algorithm's parameters. That is:
  *  For DSA: p, q, and g are generated.
  *  For RSA: nothing
@@ -3872,4 +3956,6 @@ gnutls_crypto_pk_st _gnutls_pk_ops = {
 	.pk_fixup_private_params = wrap_nettle_pk_fixup,
 	.derive = _wrap_nettle_pk_derive,
 	.curve_exists = _wrap_nettle_pk_curve_exists,
+	.pk_exists = _wrap_nettle_pk_exists,
+	.sign_exists = _wrap_nettle_pk_sign_exists
 };
diff --git a/lib/pk.h b/lib/pk.h
index cc61e08cef..7f3c9995da 100644
--- a/lib/pk.h
+++ b/lib/pk.h
@@ -40,6 +40,8 @@ extern gnutls_crypto_pk_st _gnutls_pk_ops;
 #define _gnutls_pk_generate_params( algo, bits, priv) _gnutls_pk_ops.generate_params( algo, bits, priv)
 #define _gnutls_pk_hash_algorithm( pk, sig, params, hash) _gnutls_pk_ops.hash_algorithm(pk, sig, params, hash)
 #define _gnutls_pk_curve_exists( curve) _gnutls_pk_ops.curve_exists(curve)
+#define _gnutls_pk_exists(algo) _gnutls_pk_ops.pk_exists(algo)
+#define _gnutls_pk_sign_exists(algo) _gnutls_pk_ops.sign_exists(algo)
 
 inline static int
 _gnutls_pk_fixup(gnutls_pk_algorithm_t algo, gnutls_direction_t direction,
-- 
2.34.1

